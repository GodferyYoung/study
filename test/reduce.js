/**
 * Created by YangGuo on 2016/5/29 0029.
 */

var array = [
    {
        id: 1,
        name: "小李"
    }, {
        id: 2,
        name: "小张"
    }, {
        id: 3,
        name: "小刘"
    }, {
        id: 4,
        name: "小王"
    }
]

var objMap = array.reduce(function(map,nowObj,index,array) {
    map[nowObj.id] = nowObj;
    return map;
},{});

console.log(objMap);
