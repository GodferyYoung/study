/**
 * Created by yangguo on 2016/5/24 0024.
 */
/**
 *
 * [原题及参考答案]{@link https://git.hypers.com/F2E/exam/blob/master/samples/seq.js}
 * [详细解析]{@link https://segmentfault.com/a/1190000005590411}
 */
var tasks = [1, 2, 3, 4, 5];
tasks = tasks.map(function(i) {
    return function(cb) {
        setTimeout(function() {
            console.log(i);
            cb();
        }, Math.random() * 500 | 0);
    }
});
/**
 * @callback {function} Task
 */

/**
 * 方法一
 * @param {Task []} tasks
 * @param {function} cb
 * @discard
 */
function seq(tasks, cb) {
    var i = 0;
    runTask();

    function runTask() {
        var task = tasks[i];
        task(function() {
            i++;
            if (i !== tasks.length) {
                runTask();
            } else {
                cb();
            }
        });
    }
}
//改进
function seq(tasks, cb) {
    (function runTask(i) {
        var task = tasks[i];
        task(function() {
            if (++i !== tasks.length) {
                runTask(i);
            } else {
                cb();
            }
        });
    })(0);
}

seq(tasks, function() {
    console.log('all Done');
});

/**
 * 方法二
 * @param tasks
 * @param cb
 */
function seq2(tasks, cb) {
    tasks.reduceRight(function(cb, task) {
        return function() {
            return task(cb);
        };
    }, cb)();
}

seq2(tasks, function() {
    console.log('all Done');
});
