﻿$(function () {
    //$.alertPopup({
    //    content: "呵呵"
    //});
    initView();
});

function initView() {
    //顶部下拉菜单按钮
    $("a.btn_company").click(function () {
        $("div.header > div.pull_right > ul.slide_menu").show();
        $(this).parent().find("ul.slide_menu>li>a").click(function () {
            $("div.header > div.pull_right > ul.slide_menu").hide();
        });
    });
    $('#line_wrap').highcharts({
        title: {
            text: '2014年利润报表',
            x: -20 //center
        },
        subtitle: {
            text: '公司利润报表',
            x: -20
        },
        xAxis: {
            categories: ['一月', '二月', '三月', '四月', '五月', '六月',
                '七月', '八月', '九月', '十月', '十一月', '十二月']
        },
        yAxis: {
            title: {
                text: '单位（万元）'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        credits: {//去掉右下角的友情链接
            text: '',
            href: ''
        },
        tooltip: {
            valueSuffix: '万'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '第一季度',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: '第二季度',
            data: [2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }, {
            name: '第三季度',
            data: [1, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        }, {
            name: '第四季度',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
}