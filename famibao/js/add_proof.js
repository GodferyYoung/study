﻿//记字号最大长度
const MAX_FONT_NUM_LENGTH = 3;
//附单据最大长度
const MAX_LIST_NUM_LENGTH = 3;
//数量的最大长度
const MAX_NUM_LENGTH = 3;
//单价的最大长度
const MAX_UNIT_PRICE_LENGTH = 12;
//借方、贷方金额最大长度
const MAX_DEBIT_CREDIT_PRICE_LENGTH = 12;
//金额默认值
const DEFAULT_PRICE = 0;
//数量默认值
const DEFAULT_NUM = 1;
//超过金额的最大提醒次数
var MAX_ALERT_TIMES = 1;
//金额超过限定金额时的警告信息
const ERRO_MESSAGE_MAX_PRICE = "您所输入的金额超过系统上限，请修改后重试！";
//金额超过限定金额时的警告信息
const ERRO_MESSAGE_MAX_CONUT_PRICE = "您所输入的总金额超过系统上限，借贷金额显示将异常，但最终结果仍为正确！";
//借贷不平衡时的警告信息
const ERRO_MESARGE_NOT_EQUAL = "借贷金额不平衡,请更改后再试";
//借方金额计算标识符
const COUNT_DEBIT_NAME = "debit";
//贷方金额计算标识符
const COUNT_CREDIT_NAME = "credit";
//总计借方金额
var COUNT_DEBIT_PRICE_All = 0;
//总计贷方金额
var COUNT_CREDIT_PRICE_All = 0;
//超过金额的提醒次数
var ALERT_TIMES = 0;
$(function () {
    initView();
});
//初始化页面
function initView() {
    setInput(); //设置各种input的最大输入长度
    //为记字编号输入框添加智能补全
    $("div.table_head > div.left > input.input_text_xs").blur(function () {
        var num = $(this).val();
        var n = $(this).attr("maxlength");
        var x = 0;
        $(this).val(formate(num, n, x));
    });
    //为日期输入框添加日历控件
    laydate({
        elem: '#proofdate'
    });
    //测试数据
    var availableTags = ["1001 小张", "1002 小李", "1003 小王", "1004 小宋", "1005 小赵", "1006 小郑", "1007 小明", "1008 小红", "1009 小刚", "1010 小丽", "1011 小大", "1012 小二"];

    //为输入框添加显示切换效果并对输入框进行事件绑定
    $("div.table > ul.tbody > li.tr > ul.td > li").click(function () {
        ul_td_li_click(this);
        //会计科目输入框
        $(this).find("input.input_subject").autocomplete({
            source: availableTags,
            delay: 200, //延时ms
            close: function (event, ui) { //回调事件
                input_input_subject_blur(this);
            }
        }).autocomplete("search", " ").blur(function () {
            input_input_subject_blur(this);
        });
        //摘要输入框
        $(this).find("input.input_abstract").blur(function () {//获取摘要的值放入li>span中
            input_input_subject_blur(this);
        });
        //数量、单价输入框
        $(this).find("input.input_num,input.input_unit_price").blur(function () {//获取数量单价input的值放入li>span中
            input_input_num_input_unit_price(this);
        });

        $(this).find("input.input_unit_price").keyup(function () { //监听单价输入框键盘，如果单价不为空的话则设置数量默认值
            if (validMonney(this)) {
                input_input_credit_price_keyup(this);
            }
        })

        //自动计算借方金额、贷方金额的总金额
        $(this).find("input.input_debit_price").keyup(function () {  //借方金额总计
            if (validMonney(this)) {
                count_price(COUNT_DEBIT_NAME);
            }
        });

        $(this).find("input.input_credit_price").keyup(function () {  //贷方金额总计
            if (validMonney(this)) {
                count_price(COUNT_CREDIT_NAME);
            }
        });
    });


    $("*").mouseup(function () { //点击页面任意位置 隐藏输入框
        $("div.table > ul.tbody > li.tr > ul.td > li").find("input").removeClass("wrap_block");
    });
    //新增一行
    $("i.table_btn_left.icon-add").click(function () {
        i_icon_add_click(this);
    });

    //删除一行
    $("i.table_btn_right.icon-close").click(function () {
        i_icon_close_click(this);
    });

    //新增并保存
    $("#addAndSave").click(function () {
        formatePriceToUp();
    });
    //保存
    $("#save").click(function () {
        formatePriceToUp();
    });
}

//将借贷总金额转换为大写
function formatePriceToUp() {
    if (COUNT_CREDIT_PRICE_All != COUNT_DEBIT_PRICE_All) { //判断借贷是否平衡
        $.alertPopup({
            content: ERRO_MESARGE_NOT_EQUAL,
            isSport: false
        });
        return;
    }
    $("span.span_price").empty().append(price_to_up(COUNT_CREDIT_PRICE_All / 100));
}

/*********各种监听事件*************/
//贷方金额键盘事件
function input_input_credit_price_keyup(obj) {
    var li = $(obj).parent();
    var num_li = $(li).parent().find("li>input.input_num").parent();
    if (!hasObj(num_li, "span")) {
        $(num_li).append(document.createElement("span"));
    }
    if ($(obj).val().length > 0 && !$(num_li).find("span").html().length > 0) {
        $(num_li).find("span").append(DEFAULT_NUM);
    }
    if ($(obj).val().length == 0) {
        $(num_li).find("span").empty();
    }
}
//摘要输入框blur事件
function input_input_subject_blur(obj) {
    var li = $(obj).parent();
    if (!hasObj(li, "span")) {
        $(li).append(document.createElement("span"));
    }
    $(li).find("span").empty().append($(obj).val());
}
//单价数量输入框blur事件
function input_input_num_input_unit_price(obj) {
    var li = $(obj).parent();
    if (!hasObj(li, "span")) {
        $(li).append(document.createElement("span"));
    }
    if ($(obj).hasClass("input_unit_price")) { //如果是单价则需要对数字进行保留两位小数操作
        $(li).find("span").empty();
        if (!$(obj).val().length > 0) return;
        //格式化金额
        var money = toFixed($(obj).val(), 2);
        money = money.toString().split(".")[0];
        $(li).find("span").append(money);
        return;
    }
    $(li).find("span").empty().append($(obj).val());
}
//删除按钮click事件
function i_icon_close_click(obj) {
    var $tbody = $("div.table > ul.tbody");

    if ($tbody.children("li").length == 4) return; //如果已经为4行则不能删除
    $(obj).parent().parent().parent().remove();
    if ($tbody.children("li").length == 4) {
        $tbody.css({
            "overflow-y": "initial"
        });
    };
}
//新增按钮click事件
function i_icon_add_click(obj) {
    //var $newLine = $("div.table > ul.tbody > li:last-child.tr");
    var $newLine = addALine(obj);//在页面新增一行
    $newLine.find("ul.td>li").click(function () {
        ul_td_li_click(this);
    });

    //数量、单价输入框
    $newLine.find("input.input_num,input.input_unit_price").blur(function () {//获取数量单价input的值放入li>span中
        input_input_num_input_unit_price(this);
    });

    $newLine.find("input.input_unit_price").keyup(function () { //监听单价输入框键盘，如果单价不为空的话则设置数量默认值
        if (validMonney(this)) {
            input_input_credit_price_keyup(this);
        }
    });

    //自动计算借方金额、贷方金额的总金额
    $newLine.find("input.input_debit_price").keyup(function () {  //借方金额总计
        if (validMonney(this)) {
            count_price(COUNT_DEBIT_NAME);
        }
    });

    $newLine.find("input.input_credit_price").keyup(function () {  //贷方金额总计
        if (validMonney(this)) {
            count_price(COUNT_CREDIT_NAME);
        }
    });

    $newLine.find("i.table_btn_right.icon-close").click(function () {
        i_icon_close_click(this);
    });

    $newLine.find("i.table_btn_left.icon-add").click(function () {
        i_icon_add_click(this);
    });

}
//ul.td>li的click事件
function ul_td_li_click(obj) {
    $("div.table > ul.tbody > li.tr > ul.td > li").find("input").removeClass("wrap_block");

    $(obj).find("input").addClass("wrap_block").focus(function () {
        tbody_input_focus(this);
    }).select().blur(function () {
        tbody_input_blur(this);
    });
}
//tbody中input的focus事件
function tbody_input_focus(obj) {
    if ($(obj).hasClass("input_debit_price") || $(obj).hasClass("input_credit_price")) {//光标进入根据数量、单价自动填充借方金额、贷方金额的值
        var $tr = $(obj).parent().parent().parent().parent(); //当前行
        //光标进入借方金额则清除贷方金额，否则取反
        if ($(obj).hasClass("input_debit_price")) {
            $ul = $tr.find("li.tr").last().find("ul.td");
            $ul.find("input.input_credit_price").val("");
        } else {
            $ul = $tr.find("li.tr").first().find("ul.td");
            $ul.find("input.input_debit_price").val("");
        }
        $ul.find("li").empty();
        //获取单价
        var unit_price = 0;
        if ($($tr.find("li>input.input_unit_price")).val().length > 0) {
            unit_price = $($tr.find("li>input.input_unit_price")).val();
        }
        //获取数量
        var num = $($tr.find("li>input.input_num").next("span")).html();
        //计算总金额
        var count = (isNaN(unit_price * num) ? DEFAULT_PRICE.toFixed(2) : (unit_price * num).toFixed(2));
        if (count == 0) {//如果计算总金额为0则保持原值不变 并将原值格式化为#0.00#
            $val = $(obj).val();
            $(obj).val(Number($val) == NaN ? count.toString().split(".")[0] : Number($val).toFixed(2));
            return;
        }
        $(obj).val(count);
    }
    //计算合计金额
    count_price(COUNT_DEBIT_NAME);
    count_price(COUNT_CREDIT_NAME);
    //判断其他的列如果没有span标签则添加span标签
    var li = $(obj).parent();
    if (!$(li).find("span").length > 0) return;
    $(obj).val($(li).find("span").html());
}
//tbody中input的blur事件
function tbody_input_blur(obj) {
    if ($(obj).hasClass("input_debit_price") || $(obj).hasClass("input_credit_price")) {//借方金额、贷方金额
        var $ul_li = $(obj).parent().parent().find("li");//金额填写li
        if ($(obj).val() == 0) {//为0则清空所有值,否则填充金额
            for (var i = 0 ; i < $ul_li.length; i++) {
                $($ul_li[i]).empty();
            }
            return;
        }
        var fen = parseFloat($(obj).val().toString()).toFixed(2) * 100;
        var fen_array = new Array();
        fen_array = fen.toString().split("");
        if (fen_array.length > $ul_li.length) {
            //$.alertPopup({
            //    content: ERRO_MESSAGE_MAX_PRICE,
            //    isSport: false
            //});
            //return;
            if (ALERT_TIMES < MAX_ALERT_TIMES) {
                $.alertPopup({
                    content: ERRO_MESSAGE_MAX_CONUT_PRICE,
                    isSport: false
                });
                ALERT_TIMES++;
            }
        }
        while (fen_array.length < $ul_li.length) {
            fen_array.unshift(0);
        }
        //alert(fen_array.length);
        var mark = 0; //获取第一个不为0的数字的位置
        for (var i = 0; i < fen_array.length; i++) {
            if (fen_array[i] != 0) {
                mark = i;
                break;
            }
        }
        //金额从第一个不为0的位置开始填充
        for (var i = 0 ; i < $ul_li.length; i++) {
            if (i < mark) {
                $($ul_li[i]).empty();
                continue;
            }
            $($ul_li[i]).empty().append(fen_array[i]);
        }
    }
}
//新增一行
function addALine(obj) {
    var $tbody = $("div.table > ul.tbody");
    if ($tbody.find("li").length > 4) {
        $tbody.css({
            "overflow-y": "scroll"
        });
    };
    var $parentLi = $(obj).parent().parent().parent();
    $parentLi.before("<li class=\"tr\">" +
"                                <ul class=\"td\"> " +
"                                    <li><input class=\"input_abstract\"/></li>" +
"                                    <li><input class=\"input_subject\" /></li>" +
"                                    <li><input class=\"input_unit_price\" /></li>" +
"                                    <li><input class=\"input_num\" /></li>" +
"                                    <li class=\"tr\">" +
"                                        <ul class=\"td\">" +
"                                        	<li></li>" +
"                                        	<li></li>" +
"                                            <li class=\"blue_line\"></li>" +
"                                        	<li></li>" +
"                                        	<li></li>" +
"                                            <li class=\"blue_line\"></li>" +
"                                        	<li></li>" +
"                                        	<li></li>" +
"                                            <li class=\"red_line\"></li>" +
"                                        	<li></li>" +
"                                        	<li></li>" +
"                                            <shadow>" +
"                                                <input class=\"input_debit_price\" />" +
"                                            </shadow>" +
"                                        </ul>" +
"                                    </li>" +
"                                    <li class=\"tr\">" +
"                                        <ul class=\"td\">" +
"                                            <li></li>" +
"                                            <li></li>" +
"                                            <li class=\"blue_line\"></li>" +
"                                            <li></li>" +
"                                            <li></li>" +
"                                            <li class=\"blue_line\"></li>" +
"                                            <li></li>" +
"                                            <li></li>" +
"                                            <li class=\"red_line\"></li>" +
"                                            <li></li>" +
"                                            <li></li>" +
"                                            <shadow>" +
"                                                <input class=\"input_credit_price\" />" +
"                                            </shadow>" +
"                                        </ul>" +
"                                    </li>" +
"                                    <shadow>" +
"                                        <i class=\"table_btn_left icon-add\" title=\"新增\"></i>" +
"                                        <i class=\"table_btn_right icon-close\" title=\"删除\"></i>" +
"                                    </shadow>" +
"                                </ul>" +
"                            </li>");

    return $parentLi.prev();
}

//设置各个输入框的输入显示
function setInput() {
    $("div.table_head > div.left > input.input_text_xs").attr("maxlength", MAX_FONT_NUM_LENGTH);//记字号
    $("div.table_head > div.right > input.input_text_xs").attr("maxlength", MAX_LIST_NUM_LENGTH);//附单据数量
    //表格中
    $("div.table > ul.tbody > li.tr > ul.td > li").find("input.input_num").attr("maxlength", MAX_NUM_LENGTH);//数量
    $("div.table > ul.tbody > li.tr > ul.td > li").find("input.input_unit_price").attr("maxlength", MAX_UNIT_PRICE_LENGTH);//单价
    $("div.table > ul.tbody > li.tr > ul.td > li").find("input.input_debit_price").attr("maxlength", MAX_DEBIT_CREDIT_PRICE_LENGTH);//借方金额
    $("div.table > ul.tbody > li.tr > ul.td > li").find("input.input_credit_price").attr("maxlength", MAX_DEBIT_CREDIT_PRICE_LENGTH);//贷方金额
}

/********Util Function*************/
//计算金额总计 
/*
@paramer name 
COUNT_DEBIT_NAME 借方金额
COUNT_CREDIT_NAME 贷方金额
*/
function count_price(name) {
    var $tbody = $("div.table>ul.tbody>");
    var debit_input = $tbody.find("input.input_" + name + "_price");
    var debit_array = new Array();
    //将所有的金额添加到数组中
    debit_input.each(function () {
        debit_array.push($(this).val());
    });
    //总金额
    var count_debit = Number("0");
    //遍历数组 求和
    for (var i = 0; i < debit_array.length; i++) {
        count_debit += eval((Number(debit_array[i]) == "NaN" ? 0 : Number(debit_array[i])));
    }
    //alert(count_debit);
    $ul_li = $("#count_" + name + "_price").find("li");
    var fen = parseFloat(count_debit).toFixed(2) * 100;
    fen = fen.toString().split(".")[0];
    if (name == COUNT_DEBIT_NAME) { //将总金额保存在全局变量中
        COUNT_DEBIT_PRICE_All = fen;
    } else {
        COUNT_CREDIT_PRICE_All = fen;
    }
    //$("span.span_price").empty().append(COUNT_DEBIT_PRICE_All+","+COUNT_CREDIT_PRICE_All);
    //为0则清空所有值,否则填充金额
    if (fen == 0) {
        for (var i = 0 ; i < $ul_li.length; i++) {
            $($ul_li[i]).empty();
        }
        return;
    }
    var fen_array = new Array();
    fen_array = fen.toString().split("");
    if (fen_array.length > $ul_li.length) {
        if (ALERT_TIMES < MAX_ALERT_TIMES) {
            $.alertPopup({
                content: ERRO_MESSAGE_MAX_CONUT_PRICE,
                isSport: false
            });
            ALERT_TIMES++;
        }
    }
    while (fen_array.length < $ul_li.length) {
        fen_array.unshift(0);
    }
    var mark = 0; //获取第一个不为0的数字的位置
    for (var i = 0; i < fen_array.length; i++) {
        if (fen_array[i] != 0) {
            mark = i;
            break;
        }
    }
    //金额从第一个不为0的位置开始填充
    for (var i = 0 ; i < $ul_li.length; i++) {
        if (i < mark) {
            $($ul_li[i]).empty();
            continue;
        }
        $($ul_li[i]).empty().append(fen_array[i]);
    }
}

//判断有无选择器对象
function hasObj(obj, selector) {
    if (!$(obj).find(selector).length > 0) {
        return false;
    };
    return true;
};

/*
补全字符
num 要补全的数字
n 补全的长度
x 要补的字符
*/
function formate(num, n, x) {
    var len = num.toString().length;
    while (len < n) {
        num = x + num;
        len++;
    }
    return num.toString();
}

/*
保留小数
num 要操作的数字
n 保留的位数
*/
function toFixed(num, n) {
    return Number(num).toFixed(2);
}

//判断是否为数字
function isNum(val) {
    return (val.match(/^[0-9].*$/));
}

//将金额转换为大写
function price_to_up(n) {
    if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(n))
        return "数据非法";
    var unit = "千百拾亿千百拾万千百拾圆角分", str = "";
    n += "00";
    var p = n.indexOf('.');
    if (p >= 0)
        n = n.substring(0, p) + n.substr(p + 1, 2);
    unit = unit.substr(unit.length - n.length);
    for (var i = 0; i < n.length; i++)
        str += '零壹贰叁肆伍陆柒捌玖'.charAt(n.charAt(i)) + unit.charAt(i);
    return str.replace(/零(千|百|拾|角)/g, "零").replace(/(零)+/g, "零").replace(/零(万|亿|圆)/g, "$1").replace(/(亿)万|壹(拾)/g, "$1$2").replace(/^圆零?|零分/g, "").replace(/圆$/g, "圆整");
}
//验证金额
function validMonney(obj) {
    var reg = /^(([1-9]\d*)(\.\d{0,2})?)$|(0\.{0,1}\d{0,2})$/;
    if (!reg.test($(obj).val())) {
        $(obj).val(($(obj).val().substr(0, $(obj).val().length - 1)));
        return false;
    }
    return true;
}