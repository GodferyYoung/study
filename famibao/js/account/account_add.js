﻿$(function () {
    InitView();
})

function InitView() {
    var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
    ];
    $("#express").autocomplete({
        source: availableTags
    });
    $("#fileField").change(function () {
        $("#textfield").val($(this).val());
    });
}