﻿$(function () {
    initView();
});

function initView() {
    addEditWrap();
    $("div.book_wrap>i").click(function () {
        book_wrap_i_click(this);
    });
}
//添加编辑框到页面中
function addEditWrap() {
    $("div.book_wrap").append("<div class=\"edit_wrap\">" +
"                            <div class=\"top\">" +
"                                <img src=\"images/logo_zbgl.png\" />" +
"                                <input class=\"input_text_xs\" />" +
"                            </div><!--top-->" +
"                            <div class=\"bottom\">" +
"                                <ul>" +
"                                    <li>" +
"                                        <span>建账月份：</span>" +
"                                        <span>" +
"                                            <select class=\"selct_YM_Y\" disabled>" +
"                                            </select>" +
"                                            <select class=\"selct_YM_M\" disabled>" +
"                                            </select>" +
"                                            <span>*有期初或凭证,不能修改</span>" +
"                                        </span>" +
"                                    </li>" +
"                                    <li>" +
"                                        <span>增值税：</span>" +
"                                        <input type=\"radio\" name=\"tax_type\" value=\"0\" text=\"小规模纳税人\"/><span>小规模纳税人</span>" +
"                                        <input type=\"radio\" name=\"tax_type\" value=\"1\" text=\"一般纳税人\"/><span>一般纳税人</span>" +
"                                    </li>" +
"                                    <li>" +
"                                        <span>制单人：</span>" +
"                                        <input class=\"input_text_xs\"/>" +
"                                    </li>" +
"                                </ul>" +
"                            </div>" +
"                        </div>");
    //执行封装的方法为radio添加样式
    $("input[type=radio]").cssRadio();
    $("div.book_wrap>div.edit_wrap").hide();
}
//编辑、保存按钮的事件
function book_wrap_i_click(obj) {
    var $oParent = $(obj).parent();
    var $oShow = $oParent.find("div.show_wrap");
    var $oEdit = $oParent.find("div.edit_wrap");
    //console.log($(this));
    if ($(obj).hasClass("icon-edit2")) {
        $(obj).removeClass("icon-edit2").addClass("icon-check").attr("title", "保存");
        var companyName = $oParent.find("div.show_wrap>div.top>span.company_name").html();
        var year = $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(1) > span:last-child>lable:first-child").html();
        var month = $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(1) > span:last-child>lable:last-child").html();
        var tax_type_value = $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(2) > span:last-child").attr("value");
        var username = $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(3) > span:last-child").html();
        console.log(companyName + "," + year + "," + month + "," + tax_type_value + "," + username);
        var $oEditCompanyName = $oEdit.find("div.top>input.input_text_xs");
        $oEditCompanyName.val(companyName);
        var $oEdityear = $oEdit.find("div.bottom > ul > li:nth-child(1) > span:last-child>select:nth-child(1)");
        var $oEditmonth = $oEdit.find("div.bottom > ul > li:nth-child(1) > span:last-child>select:nth-child(2)");
        console.log($oEditmonth);
        var $optYear = $("<option>").append(year + "年").attr("value", year).attr("selected", "selected").appendTo($oEdityear);
        var $optMonth = $("<option>").append(month + "月").attr("value", month).attr("selected", "selected").appendTo($oEditmonth);
        var $oRadios = $oEdit.find("input[type=radio]");
        $oRadios.each(function (index, radio) {
            if ($(radio).attr("value") == tax_type_value) {
                $(radio).parent().find("lable").click();
            }
        });
        var $oEditUserName = $oEdit.find("div.bottom > ul > li:nth-child(3)>input.input_text_xs");
        $oEditUserName.val(username);
        $oParent.find("div.show_wrap").hide();
        $oParent.find("div.edit_wrap").show();
    } else {
        $.confirmPopup({
            contain: "确认保存么？",
            func_Right: function () {
                var $oEditCompanyName = $oEdit.find("div.top>input.input_text_xs");
                var oEditCompanyNameText = $oEditCompanyName.val();
                $oParent.find("div.show_wrap>div.top>span.company_name").empty().append(oEditCompanyNameText);
                var $oRadios = $oEdit.find("input[type=radio]");
                var tax_type_value, tax_type_text;
                $oRadios.each(function (index, radio) {
                    if ($(radio).attr("checked") == "checked") {
                        tax_type_value = $(radio).attr("value");
                        tax_type_text = $(radio).attr("text");
                    }
                });
                $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(2) > span:last-child").attr("value", tax_type_value).empty().append(tax_type_text);
                var $oEditUserName = $oEdit.find("div.bottom > ul > li:nth-child(3)>input.input_text_xs");
                var userNameText = $oEditUserName.val();
                $oParent.find("div.show_wrap > div.bottom > ul > li:nth-child(3) > span:last-child").empty().append(userNameText);
                $(obj).removeClass("icon-check").addClass("icon-edit2").attr("title", "修改");
                $.alertPopup({
                    content: "修改成功",
                    func_close: function () {
                        $oParent.find("div.edit_wrap").hide();
                        $oParent.find("div.show_wrap").show();
                    }
                });
            }
        });
    }
}