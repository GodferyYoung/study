﻿$(document).ready(function () {
    //$.alertPopup({
    //    content:"你好"
    //});
    initView();
});

//初始化页面
function initView() {
    //时间段调用
    var proof_start_date = {
        elem: '#proofdate_start',
        choose: function (date) {
            proof_end_date.min = date; //开始日选好后，重置结束日的最小日期
            proof_end_date.start = date //将结束日的初始值设定为开始日
            $("#proofdate_end").click();
            $("#proofdate_end").focus();//设置光标位置
        },
        event:"click"
    };

    var proof_end_date = {
        elem: '#proofdate_end',
        choose: function (date) {
            proof_start_date.max = date; //结束日选好后，重置开始日的最大日期
        }
    };
    var bill_start_date = {
        elem: '#billdate_start',
        choose: function (date) {
            bill_end_date.min = date; //开始日选好后，重置结束日的最小日期
            bill_end_date.start = date //将结束日的初始值设定为开始日
            $("#billdate_end").click();
            $("#billdate_end").focus();//设置光标位置
        }
    };

    var bill_end_date = {
        elem: '#billdate_end',
        choose: function (date) {
            bill_start_date.max = date; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(proof_start_date);
    laydate(proof_end_date);
    laydate(bill_start_date);
    laydate(bill_end_date);
    //var year_option = {
    //    step: 1,
    //    max: 2015,
    //    min: 2000,
    //    text: "年"
    //}
    //var month_option = {
    //    step: 1,
    //    max: 12,
    //    min: 1,
    //    text: "月"
    //}
    //$("#bill_year_begin").addYearMothDay(year_option);
    //$("#bill_month_begin").addYearMothDay(month_option);
    //$("#bill_year_end").addYearMothDay(year_option);
    //$("#bill_month_end").addYearMothDay(month_option);
}