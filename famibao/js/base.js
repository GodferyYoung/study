﻿const min_height = 720; //页面最小高度
$(function () {
    init();
});

//初始化页面样式
function init() {
    if ($(window).height() > min_height) {
        $("div.footer").css({
            "position": "fixed",
            "bottom": "0px",
            "left": "0px"
        });
    }
}