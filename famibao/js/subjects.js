﻿//input提示文字
const INPUT_PROMT_TEXT = "点击此处输入内容"
$(function () {
    initView();
});

//初始化页面
function initView() {
    //为input添加输入提示
    setInput($("table.cisa_tabel_default>tbody>tr>td>input.input_text_xs"));
    //实现可编辑
    $("table.cisa_tabel_default>tbody>tr>td:nth-child(2)").click(function () {
        tbody_tr_td_2(this);
    });
    //为一级按钮添加事件
    $("table.cisa_tabel_default>tbody>tr.level_1>td>span.td_span>i.icon-add").click(function () {
        level1_addBtn_click(this);
    });
    //为二级按钮添加事件
    $("table.cisa_tabel_default>tbody>tr.level_2>td>span.td_span>i.icon-add").click(function () {
        level2_addBtn_click(this);
    });
    //为删除按钮添加事件
    $("table.cisa_tabel_default>tbody>tr>td>span.td_span>i.icon-remove2").click(function () {
        removeBtn_click(this);
    });
}
//表格中input的事件绑定
function setInput(obj) {
    $(obj).attr("placeholder", INPUT_PROMT_TEXT).css({ "color": "#CCC" }).focus(function () {
        var spanStr = $(obj).parent().find("span").html();
        if (spanStr.length > 0) {
            $(obj).val(spanStr);
            $(this).select();
        }
        $(this).css({
            "color": "#696969"
        });
    }).blur(function () {
        $(this).css({
            "color": "#949391"
        });
        inputReset();
    });
}
//表格点击事件
function tbody_tr_td_2(obj) {
    $(obj).find("span").hide();
    $(obj).find("input").show().focus().blur(function () {
        var $parent = $(this).parent();
        var $span = $parent.find("span")
        if ($(this).val().length > 0) {
            $span.empty().append($(this).val());
        } else if ($span.html().length == 0) {
            $span.append(INPUT_PROMT_TEXT);
        }
        $(this).hide();
        $span.show();
        $(this).parent().find("span").show();
    });
}
//一级新增按钮点击事件
function level1_addBtn_click(obj) {
    inputReset();
    //当前Tr
    $nowTr = $(obj).parent().parent().parent();
    //编号
    var num = $nowTr.find("td:first-child>span:first-child").html();
    //判断是否有下一个level1
    if ($nowTr.nextAll("tr.level_1").first().length > 0) {
        var $parentTr_level1_Next = $nowTr.nextAll("tr.level_1").first();
        if ($nowTr.nextUntil($parentTr_level1_Next).filter(".level_2").length > 0) {
            //同级的最后一个Tr
            var $brotherTr_level2_last = $nowTr.nextUntil($parentTr_level1_Next).filter(".level_2").last();
            //同级最后一个Tr的编号
            var lastNum = $brotherTr_level2_last.find("td:first-child>span:first-child").html();
            num = ++lastNum;
        } else {
            num = num + "01";
        }
        var domStr = getDomStr(num);
        $parentTr_level1_Next.before(domStr);
        var $newLine = $parentTr_level1_Next.prev();
    } else {//没有则直接在tbody最后追加
        if ($nowTr.nextAll().filter(".level_2").length > 0) {
            //同级的最后一个Tr
            var $brotherTr_level2_last = $nowTr.nextUntil($parentTr_level1_Next).filter(".level_2").last();
            //同级最后一个Tr的编号
            var lastNum = $brotherTr_level2_last.find("td:first-child>span:first-child").html();
            num = ++lastNum;
        } else {
            num = num + "01";
        }
        var domStr = getDomStr(num);
        $("table.cisa_tabel_default>tbody").append(domStr);
        var $newLine = $("table.cisa_tabel_default>tbody").children().last();
    }

    $newLine.find("i.icon-add").click(function () {
        level2_addBtn_click(this);
    });
    $newLine.find("i.icon-remove2").click(function () {
        removeBtn_click(this);
    });
    $newLine.find("td:nth-child(2)").click(function () {
        tbody_tr_td_2(this);
    });
    $newLine.find("td:nth-child(2)").find("span").hide();
    var newInput = $newLine.find("td:nth-child(2)").find("input.input_text_xs");
    setInput(newInput);
    $(newInput).show().focus();
    function getDomStr(num) {
        return "<tr class=\"level_2\">" +
                                "    <td class=\"cisa_tabel_default_td1\">" +
                                "        <span>" + num + "</span>" +
                                "        <span class=\"td_span\"><i class=\"icon-remove2\" title=\"删除\"></i><i class=\"icon-add\" title=\"新增\"></i></span>" +
                                "    </td>" +
                                "    <td><span></span><input class=\"input_text_xs\"></td>" +
                                "    <td>借</td>" +
                                "</tr>";
    }
}


//二级按钮点击事件
function level2_addBtn_click(obj) {
    inputReset();
    //当前Tr
    $nowTr = $(obj).parent().parent().parent();
    //编号
    var num = $nowTr.find("td:first-child>span:first-child").html();
    //当前父级的下一父级
    var $parentTr_level1_Next = $nowTr.nextAll("tr.level_1").first();
    //判断当前级到下一父级之间下是否有level_2
    if ($nowTr.nextUntil($parentTr_level1_Next).filter("tr.level_2").length > 0) {
        var $parentTr_level2_Next = $nowTr.nextAll("tr.level_2").first();
        if ($nowTr.nextUntil($parentTr_level2_Next).filter(".level_3").length > 0) {
            //同级的最后一个Tr
            var $brotherTr_level3_last = $nowTr.nextUntil($parentTr_level2_Next).filter(".level_3").last();
            //同级最后一个Tr的编号
            var lastNum = $brotherTr_level3_last.find("td:first-child>span:first-child").html();
            num = ++lastNum;
        } else {
            num = num + "01";
        }
        var domStr = getDomStr(num);
        $parentTr_level2_Next.before(domStr);
        var $newLine = $parentTr_level2_Next.prev();
    } else {
        if ($parentTr_level1_Next.length > 0) {
            if ($nowTr.nextUntil($parentTr_level1_Next).filter(".level_3").length > 0) {
                //同级的最后一个Tr
                var $brotherTr_level3_last = $nowTr.nextUntil($parentTr_level1_Next).filter(".level_3").last();
                //同级最后一个Tr的编号
                var lastNum = $brotherTr_level3_last.find("td:first-child>span:first-child").html();
                num = ++lastNum;
            } else {
                num = num + "01";
            }
        } else {
            if ($nowTr.nextAll("tr.level_3").length > 0) {
                var lastNum = $nowTr.nextAll("tr.level_3").last().find("td:first-child>span:first-child").html();
                num = ++lastNum;
            } else {
                num = num + "01";
            }
        }

        var domStr = getDomStr(num);
        //当前行有下级
        if ($parentTr_level1_Next.length > 0) {
            $parentTr_level1_Next.before(domStr);
            var $newLine = $parentTr_level1_Next.prev();
        } else {
            $("table.cisa_tabel_default>tbody").append(domStr);
            var $newLine = $("table.cisa_tabel_default>tbody").children().last();
        }
    }

    $newLine.find("td:nth-child(2)").click(function () {
        tbody_tr_td_2(this);
    });
    $newLine.find("i.icon-remove2").click(function () {
        removeBtn_click(this);
    });
    $newLine.find("td:nth-child(2)").find("span").hide();
    var newInput = $newLine.find("td:nth-child(2)").find("input.input_text_xs");
    setInput(newInput);
    $(newInput).show().focus();

    function getDomStr(num) {
        return "<tr class=\"level_3\">" +
                "    <td class=\"cisa_tabel_default_td2\">" +
                "        <span>" + num + "</span>" +
                "<span class=\"td_span\"><i class=\"icon-remove2\" title=\"删除\"></i></span>" +
                "    </td>" +
                "    <td><span></span><input class=\"input_text_xs\"></td>" +
                "    <td>借</td>" +
                "</tr>";
    }
}

//删除按钮事件
function removeBtn_click(obj) {
    var $nowTr = $(obj).parent().parent().parent();
    $.confirmPopup({
        isSport: false, //有无动画
        func_Right: function () {
            $nowTr.remove();
        }
    });
}

//重置所有的输入框显示
function inputReset() {
    $("table.cisa_tabel_default>tbody>tr>td:nth-child(2)").find("span").show();
    $("table.cisa_tabel_default>tbody>tr>td:nth-child(2)").find("input").hide();
}