﻿$(function () {
    initView();
});

//初始化页面
function initView() {
    var start_date = {
        elem: '#date_start',
        choose: function (date) {
            end_date.min = date; //开始日选好后，重置结束日的最小日期
            end_date.start = date //将结束日的初始值设定为开始日
            $("#date_end").click();
            $("#date_end").focus();//设置光标位置
        }
    };

    var end_date = {
        elem: '#date_end',
        choose: function (date) {
            bill_start_date.max = date; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(start_date);
    laydate(end_date);

    $("#timeShaft").timeShaft({
        month: 5,
        year: 2015,
        func: function () {
            $.alertPopup({
                content: "测试"
            });
        }
    });
}